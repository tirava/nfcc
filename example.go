package main

import (
	"fmt"
	"log"

	"gitlab.com/tirava/nfcc/pkg/adapters"
	"gitlab.com/tirava/nfcc/pkg/client"
	"gitlab.com/tirava/nfcc/pkg/commands"
	"gitlab.com/tirava/nfcc/pkg/events"
	"gitlab.com/tirava/nfcc/pkg/jobs"
	"gitlab.com/tirava/nfcc/pkg/runs"
	"gitlab.com/tirava/nfcc/pkg/snippets"
	"gitlab.com/tirava/nfcc/pkg/sort"
	"gitlab.com/tirava/nfcc/pkg/tags"
)

const server = "https://www.fake.com"

// examples
// nolint:funlen
func main() {
	fmt.Println("Adapter Types:", adapters.NFC, adapters.BarCode, adapters.BlueTooth)
	fmt.Println("Snippet Categories:", snippets.Tag, snippets.Adapter)
	fmt.Println("Tag Types:", tags.NFC, tags.BarCode, tags.BlueTooth)
	fmt.Println("Event Names:", events.TagDiscovery, "...", events.RunSuccess)
	fmt.Println("Sort Fields:", sort.CreatedAt)
	fmt.Println("Sort Directions:", sort.Asc, sort.Desc)

	cl := client.New(server)

	fmt.Println("WebSocket endpoint:", cl.WebSocket.EndPoint())

	if _, err := cl.Adapters.ListAll(adapters.BarCode); err != nil {
		log.Println("Adapters.ListAll(),", err)
	}

	if _, err := cl.Adapters.GetDetails("111-222-333"); err != nil {
		log.Println("Adapters.GetDetails(),", err)
	}

	if _, err := cl.About.GetServiceInfo(); err != nil {
		log.Println("About.GetServiceInfo(),", err)
	}

	if _, err := cl.Snippets.ListAll(snippets.Adapter, "111"); err != nil {
		log.Println("Snippets.ListAll(),", err)
	}

	if _, err := cl.Tags.ListAll(tags.BlueTooth, "111-222-333"); err != nil {
		log.Println("Tags.ListAll(),", err)
	}

	if _, err := cl.Tags.GetDetails("111-222-333", "1111-2222-3333"); err != nil {
		log.Println("Tags.GetDetails(),", err)
	}

	if _, err := cl.Events.ListAll(events.JobActivated, "111-222-333",
		111, 222, sort.CreatedAt, sort.Desc); err != nil {
		log.Println("Events.ListAll,", err)
	}

	if _, err := cl.Events.Send(events.NewEvent{
		Name: events.AdapterRelease,
		Data: adapters.Adapter{Driver: "555"},
	}); err != nil {
		log.Println("Events.Send(),", err)
	}

	if _, err := cl.Runs.ListAll("111-222-333", "333-222-111", runs.Success,
		sort.CreatedAt, sort.Desc, 111, 222); err != nil {
		log.Println("Runs.ListAll(),", err)
	}

	if _, err := cl.Runs.GetDetails("111-222-333", "11111-22222-33333"); err != nil {
		log.Println("Runs.GetDetails(),", err)
	}

	if _, err := cl.Jobs.ListAll("111-222-333", jobs.Active,
		sort.SuccessRuns, sort.Asc, 111, 222); err != nil {
		log.Println("Jobs.ListAll(),", err)
	}

	if _, err := cl.Jobs.Send("111-222-333", jobs.NewJob{
		JobName: "qqq-www-eee",
		Steps: []jobs.JobStep{
			{
				Command: commands.ReadNdef,
				Params:  commands.CommandParamsReadNdef{},
			},
			{
				Command: commands.TransmitTag,
				Params:  commands.CommandParamsTransmitTag{TxBytes: "base64-333"},
			},
			{
				Command: commands.LockPermanent,
				Params:  commands.CommandParamsLockPermanent{},
			},
		},
	}); err != nil {
		log.Println("Jobs.Send(),", err)
	}

	if err := cl.Jobs.DeleteAll("111-222-333"); err != nil {
		log.Println("Jobs.DeleteAll(),", err)
	}

	if err := cl.Jobs.Delete("111-222-333", "333-222-111"); err != nil {
		log.Println("Jobs.Delete(),", err)
	}

	if _, err := cl.Jobs.GetDetails("111-222-333", "3333-2222-1111"); err != nil {
		log.Println("Jobs.JobDetails(),", err)
	}

	if _, err := cl.Jobs.UpdateStatus("111-222-333", "3333-2222-1111", jobs.Pending); err != nil {
		log.Println("Jobs.Update(),", err)
	}
}
