package jobs

import "gitlab.com/tirava/nfcc/pkg/commands"

// JobStep object.
type JobStep struct {
	// Command for job.
	Command string                 `json:"command"`
	Params  commands.CommandParams `json:"params"`
}

// NewJob object.
type NewJob struct {
	// Job name.
	JobName string `json:"job_name"`
	// Specify how many times job should be repeated.
	Repeat int `json:"repeat"`
	// Time in seconds to expire job.
	ExpireAfter int `json:"expire_after"`
	// Steps of job.
	Steps []JobStep `json:"steps"`
}

// Job object.
type Job struct {
	NewJob
	// Unique identifier in form of UUID representing a specific job.
	JobID string `json:"job_id"`
	// Type of API object.
	Kind string `json:"kind"`
	// Link for requsted API resource.
	Href string `json:"href"`
	// Unique identifier in form of UUID representing a specific adapter.
	AdapterID string `json:"adapter_id"`
	// Adapter name.
	AdapterName string `json:"adapter_name"`
	// Job status.
	Status string `json:"status"`
	// Specify how many times job runned.
	TotalRuns int `json:"total_runs"`
	// Specify how many times job was successfully runned.
	SuccessRuns int `json:"success_runs"`
	// Specify how many times job was runned with errors.
	ErrorRuns int `json:"error_runs"`
	// CreatedAt in date-time format
	CreatedAt string `json:"created_at"`
}

// JobList object.
type JobList struct {
	// Total number of jobs.
	Total int `json:"total"`
	// Number of jobs in list.
	Length int `json:"length"`
	// Limit for number of jobs in list.
	Limit int `json:"limit"`
	// Offset for list.
	Offset int `json:"offset"`
	// Job array.
	Items []Job `json:"items"`
}

// JobStatusPatch object.
type JobStatusPatch struct {
	// Job status.
	Status string `json:"status"`
}
