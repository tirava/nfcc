// Package jobs implements jobs operations.
package jobs

import (
	"fmt"
	"strings"

	"gitlab.com/tirava/nfcc/pkg/commands"
	"gitlab.com/tirava/nfcc/pkg/errors"
)

const endPoint = "/adapters/{adapter_id}/jobs"

// Jobs base type.
type Jobs struct {
	fullURL string
}

// New returns new jobs.
func New(server string) *Jobs {
	return &Jobs{
		fullURL: fmt.Sprintf("%s%s", server, endPoint),
	}
}

func (j Jobs) insertAdapterID(adapterID string) string {
	return strings.Replace(j.fullURL, "{adapter_id}", adapterID, 1)
}

// ListAll returns list of jobs (parameters - optional, except adapterID).
func (j Jobs) ListAll(adapterID, status, sortBy, sortDir string, limit, offset int) (*JobList, error) {
	respErr := &errors.Error{
		Message: "not implemented",
		Info: fmt.Sprintf("%s, adapter=%s, status=%s, sortBy=%s, sortDir=%s, limit=%d, offset=%d",
			j.insertAdapterID(adapterID), adapterID, status, sortBy, sortDir, limit, offset),
	}

	return nil, respErr.Err()
}

// Send job with list of steps to adapter.
func (j Jobs) Send(adapterID string, job NewJob) (*Job, error) {
	if err := j.validateJobStepCommandParams(job.Steps); err != nil {
		return nil, err
	}

	respErr := &errors.Error{
		Message: "not implemented",
		Info:    fmt.Sprintf("%s, name=%s", j.insertAdapterID(adapterID), job.JobName),
	}

	return nil, respErr.Err()
}

// DeleteAll deletes all jobs from adapter.
func (j Jobs) DeleteAll(adapterID string) error {
	respErr := &errors.Error{
		Message: "not implemented",
		Info:    fmt.Sprintf("%s, adapter=%s", j.insertAdapterID(adapterID), adapterID),
	}

	return respErr.Err()
}

// GetDetails returns specific job details.
func (j Jobs) GetDetails(adapterID, jobID string) (*Job, error) {
	respErr := &errors.Error{
		Message: "not implemented",
		Info:    fmt.Sprintf("%s/%s", j.insertAdapterID(adapterID), jobID),
	}

	return &Job{}, respErr.Err()
}

// UpdateStatus updates job status in adapter.
func (j Jobs) UpdateStatus(adapterID, jobID, status string) (*Job, error) {
	respErr := &errors.Error{
		Message: "not implemented",
		Info:    fmt.Sprintf("%s/%s, status=%s", j.insertAdapterID(adapterID), jobID, status),
	}

	return &Job{}, respErr.Err()
}

// Delete deletes job from adapter.
func (j Jobs) Delete(adapterID, jobID string) error {
	respErr := &errors.Error{
		Message: "not implemented",
		Info:    fmt.Sprintf("%s, adapter=%s, job=%s", j.insertAdapterID(adapterID), adapterID, jobID),
	}

	return respErr.Err()
}

// nolint:funlen
func (j Jobs) validateJobStepCommandParams(steps []JobStep) error {
	var s string

	for _, step := range steps {
		name := step.Command

		switch step.Params.(type) {
		case commands.CommandParamsGetTags:
			if name != commands.GetTags {
				s = "CommandParamsGetTags"
			}
		case commands.CommandParamsTransmitAdapter:
			if name != commands.TransmitAdapter {
				s = "CommandParamsTransmitAdapter"
			}
		case commands.CommandParamsTransmitTag:
			if name != commands.TransmitTag {
				s = "CommandParamsTransmitTag"
			}
		case commands.CommandParamsReadNdef:
			if name != commands.ReadNdef {
				s = "CommandParamsReadNdef"
			}
		case commands.CommandParamsWriteNdef:
			if name != commands.WriteNdef {
				s = "CommandParamsWriteNdef"
			}
		case commands.CommandParamsLockPermanent:
			if name != commands.LockPermanent {
				s = "CommandParamsLockPermanent"
			}
		case commands.CommandParamsSetPassword:
			if name != commands.SetPassword {
				s = "CommandParamsSetPassword"
			}
		case commands.CommandParamsRemovePassword:
			if name != commands.RemovePassword {
				s = "CommandParamsRemovePassword"
			}
		case commands.CommandParamsFormatDefault:
			if name != commands.FormatDefault {
				s = "CommandParamsFormatDefault"
			}
		case commands.CommandParamsGetDump:
			if name != commands.GetDump {
				s = "CommandParamsGetDump"
			}
		case commands.CommandParamsAuthPassword:
			if name != commands.AuthPassword {
				s = "CommandParamsAuthPassword"
			}
		default:
			return errors.NFCCError("invalid step command type")
		}

		if s != "" {
			return errors.NFCCError(fmt.Sprintf("Command name '%s' not equal '%s' command type", name, s))
		}
	}

	return nil
}
