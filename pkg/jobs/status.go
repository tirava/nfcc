package jobs

// JobStatus enums.
const (
	Pending = "pending"
	Active  = "active"
)
