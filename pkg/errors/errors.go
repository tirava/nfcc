// Package errors implements errors types.
package errors

import "fmt"

// NFCCError object.
type NFCCError string

// Error is errorer for NFCCError.
func (n NFCCError) Error() string {
	return string(n)
}

// Error object.
type Error struct {
	Message string `json:"error_message"`
	Info    string `json:"error_info"`
}

// Err return error from Error object.
func (e Error) Err() error {
	return NFCCError(fmt.Sprintf("message: %s, info: %s", e.Message, e.Info))
}
