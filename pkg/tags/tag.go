package tags

// TagList object.
type TagList []TagShort

// TagShort object.
type TagShort struct {
	// Unique identifier in form of UUID representing a specific tag.
	TagID string `json:"tag_id"`
	// Type of API object.
	Kind string `json:"kind"`
	// Link for requested API resource.
	Href string `json:"href"`
	// Adapter type.
	Type string `json:"type"`
	// Unique identifier in form of UUID representing a specific adapter.
	AdapterID string `json:"adapter_id"`
	// Adapter name.
	AdapterName string `json:"adapter_name"`
	// Tag uid.
	UID string `json:"uid"`
}

// Tag object.
type Tag struct {
	TagShort
	// Tag atr.
	Atr string `json:"atr"`
	// Tag IC name.
	Product string `json:"product"`
	// Manufacturer of tag.
	Vendor string `json:"vendor"`
}
