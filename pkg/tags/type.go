package tags

// TagType enums.
const (
	NFC       = "nfc"
	BarCode   = "barcode"
	BlueTooth = "bluetooth"
)
