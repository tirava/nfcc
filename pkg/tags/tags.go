// Package tags implements tags info.
package tags

import (
	"fmt"
	"strings"

	"gitlab.com/tirava/nfcc/pkg/errors"
)

const endPoint = "/adapters/{adapter_id}/tags"

// Tags base type.
type Tags struct {
	fullURL string
}

// New returns new adapters.
func New(server string) *Tags {
	return &Tags{
		fullURL: fmt.Sprintf("%s%s", server, endPoint),
	}
}

func (t Tags) insertAdapterID(adapterID string) string {
	return strings.Replace(t.fullURL, "{adapter_id}", adapterID, 1)
}

// ListAll returns all adapter tags (optional - given type).
// The response includes array of Tags.
func (t Tags) ListAll(tagType, adapterID string) (*TagList, error) {
	respErr := &errors.Error{
		Message: "not implemented",
		Info:    fmt.Sprintf("%s, type=%s", t.insertAdapterID(adapterID), tagType),
	}

	return nil, respErr.Err()
}

// GetDetails returns tag with all details.
func (t Tags) GetDetails(adapterID, tagID string) (*Tag, error) {
	respErr := &errors.Error{
		Message: "not implemented",
		Info:    fmt.Sprintf("%s/%s", t.insertAdapterID(adapterID), tagID),
	}

	return &Tag{}, respErr.Err()
}
