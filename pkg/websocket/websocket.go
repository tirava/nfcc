// Package websocket implements websocket events listening.
package websocket

import (
	"fmt"
)

const endPoint = "/ws"

// WebSocket base type.
type WebSocket struct {
	fullURL string
}

// New returns new web socket item.
func New(server string) *WebSocket {
	return &WebSocket{
		fullURL: fmt.Sprintf("%s%s", server, endPoint),
	}
}

// EndPoint returns full endpoint to websocket connection.
func (w WebSocket) EndPoint() string {
	return w.fullURL
}
