// Package client implements all client operations.
package client

import (
	"gitlab.com/tirava/nfcc/pkg/about"
	"gitlab.com/tirava/nfcc/pkg/adapters"
	"gitlab.com/tirava/nfcc/pkg/events"
	"gitlab.com/tirava/nfcc/pkg/jobs"
	"gitlab.com/tirava/nfcc/pkg/runs"
	"gitlab.com/tirava/nfcc/pkg/snippets"
	"gitlab.com/tirava/nfcc/pkg/tags"
	"gitlab.com/tirava/nfcc/pkg/websocket"
)

// Client base type.
type Client struct {
	WebSocket *websocket.WebSocket
	Adapters  *adapters.Adapters
	About     *about.About
	Snippets  *snippets.Snippets
	Tags      *tags.Tags
	Events    *events.Events
	Runs      *runs.Runs
	Jobs      *jobs.Jobs
}

// New returns new client bundle.
func New(server string) *Client {
	return &Client{
		WebSocket: websocket.New(server),
		Adapters:  adapters.New(server),
		About:     about.New(server),
		Snippets:  snippets.New(server),
		Tags:      tags.New(server),
		Events:    events.New(server),
		Runs:      runs.New(server),
		Jobs:      jobs.New(server),
	}
}
