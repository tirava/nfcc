// Package dumps implements dumps operations.
package dumps

// PageDump object.
type PageDump struct {
	// Page number.
	Page string `json:"page"`
	// Tag memory data bytes.
	Data string `json:"data"`
	// Additional information about tag memory data bytes.
	Info string `json:"info"`
}

// MemoryDump tag.
type MemoryDump []PageDump
