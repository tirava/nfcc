// Package snippets implements snippets info.
package snippets

import (
	"fmt"

	"gitlab.com/tirava/nfcc/pkg/errors"
)

const endPoint = "/snippets"

// Snippets base type.
type Snippets struct {
	fullURL string
}

// New returns new snippets.
func New(server string) *Snippets {
	return &Snippets{
		fullURL: fmt.Sprintf("%s%s", server, endPoint),
	}
}

// ListAll returns information about all snippets (optional - given category and usage_id).
// The response includes array of Snippets.
func (s Snippets) ListAll(snipCat, usageID string) (*SnippetList, error) {
	respErr := &errors.Error{
		Message: "not implemented",
		Info:    fmt.Sprintf("%s, category=%s, usage_id=%s", s.fullURL, snipCat, usageID),
	}

	return nil, respErr.Err()
}
