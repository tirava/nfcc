package snippets

// SnippetList object.
type SnippetList []Snippet

// Snippet object.
type Snippet struct {
	// Name of snippet.
	Name string `json:"name"`
	// Categories for snippets.
	Category string `json:"category"`
	// Tag or adapter title this snippet is used for.
	UsageName string `json:"usage_name"`
	// Tag or adapter id this snippet is used for.
	UsageID string `json:"usage_id"`
	// Description for snippet.
	Description string `json:"description"`
	// Snippet's code
	Code string `json:"code"`
}
