package adapters

// AdapterType enums.
const (
	NFC       = "nfc"
	BarCode   = "barcode"
	BlueTooth = "bluetooth"
)
