// Package adapters implements adapters info.
package adapters

import (
	"fmt"

	"gitlab.com/tirava/nfcc/pkg/errors"
)

const endPoint = "/adapters"

// Adapters base type.
type Adapters struct {
	fullURL string
}

// New returns new adapters.
func New(server string) *Adapters {
	return &Adapters{
		fullURL: fmt.Sprintf("%s%s", server, endPoint),
	}
}

// ListAll returns information about all adapters (optional - given type).
// The response includes array of Adapters.
func (a Adapters) ListAll(adapterType string) (*AdapterList, error) {
	respErr := &errors.Error{
		Message: "not implemented",
		Info:    fmt.Sprintf("%s, type=%s", a.fullURL, adapterType),
	}

	return nil, respErr.Err()
}

// GetDetails returns adapter with all details.
func (a Adapters) GetDetails(adapterID string) (*Adapter, error) {
	respErr := &errors.Error{
		Message: "not implemented",
		Info:    fmt.Sprintf("%s/%s", a.fullURL, adapterID),
	}

	return &Adapter{}, respErr.Err()
}
