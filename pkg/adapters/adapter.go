package adapters

// AdapterList object.
type AdapterList []AdapterShort

// AdapterShort object.
type AdapterShort struct {
	// Unique identifier in form of UUID representing a specific adapter.
	AdapterID string `json:"adapter_id"`
	// Type of API object.
	Kind string `json:"kind"`
	// Link for requested API resource.
	Href string `json:"href"`
	// Adapter name.
	Name string `json:"name"`
	// Adapter type.
	Type string `json:"type"`
}

// Adapter object.
type Adapter struct {
	AdapterShort
	// Adapter driver name.
	Driver string `json:"driver"`
}
