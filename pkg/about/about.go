// Package about implements about info.
package about

import (
	"fmt"

	"gitlab.com/tirava/nfcc/pkg/errors"
)

const endPoint = "/about"

// About base type.
type About struct {
	fullURL string
}

// New returns new about.
func New(server string) *About {
	return &About{
		fullURL: fmt.Sprintf("%s%s", server, endPoint),
	}
}

// GetServiceInfo returns information about service.
func (a About) GetServiceInfo() (*ServiceInfo, error) {
	respErr := &errors.Error{
		Message: "not implemented",
		Info:    a.fullURL,
	}

	return &ServiceInfo{}, respErr.Err()
}
