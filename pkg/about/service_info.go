package about

// ServiceInfo object about.
type ServiceInfo struct {
	// Name of service server.
	Name string `json:"name"`
	// Current version of server.
	Version string `json:"version"`
	// Service build commit.
	Commit string `json:"commit"`
	// SDK version.
	SDKInfo string `json:"sdk_info"`
	// Build time.
	BuildTime string `json:"build_time"`
	// Latest version of server.
	UpdateVersion string `json:"update_version"`
	// Download link to latest version of server.
	UpdateDownload string `json:"update_download"`
	// Start time
	StartedAt string `json:"started_at"`
}
