package runs

import (
	"gitlab.com/tirava/nfcc/pkg/commands"
	"gitlab.com/tirava/nfcc/pkg/tags"
)

// JobRun object.
type JobRun struct {
	// Unique identifier in form of UUID representing a specific run.
	RunID string `json:"run_id"`
	// Type of API object.
	Kind string `json:"kind"`
	// Link for requested API resource.
	Href string `json:"href"`
	// Unique identifier in form of UUID representing a specific job.
	JobID string `json:"job_id"`
	// Job name.
	JobName string `json:"job_name"`
	// Unique identifier in form of UUID representing a specific adapter.
	AdapterID string `json:"adapter_id"`
	// Adapter name.
	AdapterName string `json:"adapter_name"`
	// Job Run status.
	Status string   `json:"status"`
	Tag    tags.Tag `json:"tag"`
	// Result of run.
	Results []commands.StepResult `json:"results"`
	// CreatedAt in date-time format
	CreatedAt string `json:"created_at"`
}

// JobRunList object.
type JobRunList struct {
	// Total number of runs.
	Total int `json:"total"`
	// Number of runs in list.
	Length int `json:"length"`
	// Limit for number of runs in list.
	Limit int `json:"limit"`
	// Offset for list.
	Offset int `json:"offset"`
	// Job Run array.
	Items []JobRun `json:"items"`
}
