package runs

// JobRunStatus enums.
const (
	Started = "started"
	Success = "success"
	Error   = "error"
)
