// Package runs implements runs info.
package runs

import (
	"fmt"
	"strings"

	"gitlab.com/tirava/nfcc/pkg/errors"
)

const endPoint = "/adapters/{adapter_id}/runs"

// Runs base type.
type Runs struct {
	fullURL string
}

// New returns new runs.
func New(server string) *Runs {
	return &Runs{
		fullURL: fmt.Sprintf("%s%s", server, endPoint),
	}
}

func (r Runs) insertAdapterID(adapterID string) string {
	return strings.Replace(r.fullURL, "{adapter_id}", adapterID, 1)
}

// ListAll returns list of job runs (parameters - optional, except adapterID).
func (r Runs) ListAll(adapterID, jobID, status, sortBy, sortDir string, limit, offset int) (*JobRunList, error) {
	respErr := &errors.Error{
		Message: "not implemented",
		Info: fmt.Sprintf("%s, adapter=%s, job=%s, status=%s, sortBy=%s, sortDir=%s, limit=%d, offset=%d",
			r.insertAdapterID(adapterID), adapterID, jobID, status, sortBy, sortDir, limit, offset),
	}

	return nil, respErr.Err()
}

// GetDetails returns job run with all details.
func (r Runs) GetDetails(adapterID, runID string) (*JobRun, error) {
	respErr := &errors.Error{
		Message: "not implemented",
		Info:    fmt.Sprintf("%s/%s", r.insertAdapterID(adapterID), runID),
	}

	return &JobRun{}, respErr.Err()
}
