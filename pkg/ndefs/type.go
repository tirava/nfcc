package ndefs

// NDEF record payload enums.
const (
	Raw    = "raw"
	URL    = "url"
	Text   = "text"
	URI    = "uri"
	VCard  = "vcard"
	MIME   = "mime"
	Phone  = "phone"
	Geo    = "geo"
	AAR    = "aar"
	Poster = "poster"
)
