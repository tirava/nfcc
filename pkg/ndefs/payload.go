package ndefs

// NdefRecordPayload implements payloads.
type NdefRecordPayload interface{}

// NdefRecordPayloadRaw record for raw payload.
type NdefRecordPayloadRaw struct {
	// TNF value of NDEF record payload.
	TNF int `json:"tnf"`
	// Type value of NDEF record payload.
	Type string `json:"type"`
	// ID value of NDEF record payload.
	ID string `json:"id"`
	// payload value of NDEF record payload.
	Payload string `json:"payload"`
}

// NdefRecordPayloadURL record for url payload.
type NdefRecordPayloadURL struct {
	// URL value of NDEF record payload.
	URL string `json:"url"`
}

// NdefRecordPayloadText record for text payload.
type NdefRecordPayloadText struct {
	// Text value of NDEF record payload.
	Text string `json:"text"`
	// Language value of NDEF record payload.
	Language string `json:"lang"`
}

// NdefRecordPayloadURI record for uri payload.
type NdefRecordPayloadURI struct {
	// URI value of NDEF record payload.
	URI string `json:"uri"`
}

// NdefRecordPayloadVcard record for vcard payload.
type NdefRecordPayloadVcard struct {
	// Address city value of NDEF record payload.
	AddressCity string `json:"address_city"`
	// Address country value of NDEF record payload.
	AddressCountry string `json:"address_country"`
	// Address postal code value of NDEF record payload.
	AddressPostalCode string `json:"address_postal_code"`
	// Address region value of NDEF record payload
	AddressRegion string `json:"address_region"`
	// Address street value of NDEF record payload.
	AddressStreet string `json:"address_street"`
	// Email value of NDEF record payload.
	Email string `json:"email"`
	// First name value of NDEF record payload.
	FirstName string `json:"first_name"`
	// Last name value of NDEF record payload.
	LastName string `json:"last_name"`
	// Organization value of NDEF record payload.
	Organization string `json:"organization"`
	// Phone cell value of NDEF record payload.
	PhoneCell string `json:"phone_cell"`
	// Phone home value of NDEF record payload.
	PhoneHome string `json:"phone_home"`
	// Phone work value of NDEF record payload.
	PhoneWork string `json:"phone_work"`
	// Title value of NDEF record payload.
	Title string `json:"title"`
	// Site value of NDEF record payload
	Site string `json:"site"`
}

// NdefRecordPayloadMime record for mime payload.
type NdefRecordPayloadMime struct {
	// Content value of NDEF record payload.
	Content string `json:"content"`
	// Format value of NDEF record payload. Could "Hex" or "ASCII".
	Format string `json:"format"`
	// Content type value of NDEF record payload.
	ContentType string `json:"content_type"`
}

// NdefRecordPayloadPhone record for phone payload.
type NdefRecordPayloadPhone struct {
	// Phone number value of NDEF record payload.
	PhoneNumber string `json:"phone_number"`
}

// NdefRecordPayloadGeo record for geo payload.
type NdefRecordPayloadGeo struct {
	// Latitude value of NDEF record payload.
	Latitude string `json:"latitude"`
	// Longitude type value of NDEF record payload.
	Longitude string `json:"longitude"`
}

// NdefRecordPayloadAar record for aar payload.
type NdefRecordPayloadAar struct {
	// Package name value of NDEF record payload.
	PackageName string `json:"package_name"`
}

// NdefRecordPayloadPoster record for poster payload.
type NdefRecordPayloadPoster struct {
	// Title value of NDEF record payload.
	Title string `json:"title"`
	// URI type value of NDEF record payload.
	URI string `json:"uri"`
}
