// Package ndefs implements ndef operations..
package ndefs

// NdefRecord object.
type NdefRecord struct {
	// NDEF record payload type.
	Type string            `json:"type"`
	Data NdefRecordPayload `json:"data"`
}

// Ndef object.
type Ndef struct {
	// Is tag could be writted.
	ReadOnly bool `json:"read_only"`
	// NDEF records.
	Message []NdefRecord `json:"message"`
}
