// Package sort implements sort types.
package sort

// Field enums.
const (
	Name        = "name"
	TotalRuns   = "total_runs"
	SuccessRuns = "success_runs"
	ErrorRuns   = "error_runs"
	Repeat      = "repeat"
	CreatedAt   = "created_at"
)

// Direction enums.
const (
	Asc  = "asc"
	Desc = "desc"
)
