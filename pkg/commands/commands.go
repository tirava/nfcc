// Package commands implements commands operations.
package commands

// Command names.
const (
	GetTags         = "get_tags"
	TransmitAdapter = "transmit_adapter"
	TransmitTag     = "transmit_tag"
	WriteNdef       = "write_ndef"
	ReadNdef        = "read_ndef"
	FormatDefault   = "format_default"
	LockPermanent   = "lock_permanent"
	SetPassword     = "set_password"
	RemovePassword  = "remove_password"
	GetDump         = "get_dump"
	AuthPassword    = "auth_password"
)
