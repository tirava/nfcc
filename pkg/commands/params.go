package commands

import "gitlab.com/tirava/nfcc/pkg/ndefs"

// CommandParams implements command parameters.
type CommandParams interface{}

// CommandParamsGetTags params.
type CommandParamsGetTags struct {
}

// CommandParamsTransmitAdapter params.
type CommandParamsTransmitAdapter struct {
	// rx bytes in base64 string
	TxBytes string `json:"tx_bytes"`
}

// CommandParamsTransmitTag params.
type CommandParamsTransmitTag struct {
	// rx bytes in base64 string
	TxBytes string `json:"tx_bytes"`
}

// CommandParamsReadNdef params.
type CommandParamsReadNdef struct {
}

// CommandParamsWriteNdef params.
type CommandParamsWriteNdef struct {
	// NDEF records.
	Message []ndefs.NdefRecord `json:"message"`
}

// CommandParamsLockPermanent params.
type CommandParamsLockPermanent struct {
}

// CommandParamsSetPassword params.
type CommandParamsSetPassword struct {
	// Password to set.
	Password string `json:"password"`
}

// CommandParamsRemovePassword params.
type CommandParamsRemovePassword struct {
	// Password to remove.
	Password string `json:"password"`
}

// CommandParamsFormatDefault params.
type CommandParamsFormatDefault struct {
}

// CommandParamsGetDump params.
type CommandParamsGetDump struct {
}

// CommandParamsAuthPassword params.
type CommandParamsAuthPassword struct {
	// Auth password.
	Password string `json:"password"`
}
