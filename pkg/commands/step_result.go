package commands

// StepResult object.
type StepResult struct {
	// Command for job.
	Command string `json:"command"`
	// One of the params types.
	Params CommandParams `json:"params"`
	// One of the output types.
	Output CommandOutput `json:"output"`
	// Command status.
	Status string `json:"status"`
	// Result description.
	Message string `json:"message"`
}
