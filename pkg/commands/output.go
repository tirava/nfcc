package commands

import (
	"gitlab.com/tirava/nfcc/pkg/dumps"
	"gitlab.com/tirava/nfcc/pkg/ndefs"
	"gitlab.com/tirava/nfcc/pkg/tags"
)

// CommandOutput implements command response output.
type CommandOutput interface{}

// CommandOutputGetTags response params.
type CommandOutputGetTags tags.TagList

// CommandOutputTransmitAdapter response params.
type CommandOutputTransmitAdapter struct {
	// rx bytes in base64 string
	TxBytes string `json:"tx_bytes"`
}

// CommandOutputTransmitTag response params.
type CommandOutputTransmitTag struct {
	// rx bytes in base64 string
	TxBytes string `json:"tx_bytes"`
}

// CommandOutputReadNdef response params.
type CommandOutputReadNdef ndefs.Ndef

// CommandOutputWriteNdef response params.
type CommandOutputWriteNdef struct {
}

// CommandOutputLockPermanent response params.
type CommandOutputLockPermanent struct {
}

// CommandOutputSetPassword response params.
type CommandOutputSetPassword struct {
}

// CommandOutputRemovePassword response params.
type CommandOutputRemovePassword struct {
}

// CommandOutputFormatDefault response params.
type CommandOutputFormatDefault struct {
}

// CommandOutputGetDump response params.
type CommandOutputGetDump dumps.MemoryDump

// CommandOutputAuthPassword response params.
type CommandOutputAuthPassword struct {
}
