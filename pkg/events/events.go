// Package events implements events operations.
package events

import (
	"fmt"

	"gitlab.com/tirava/nfcc/pkg/runs"

	"gitlab.com/tirava/nfcc/pkg/adapters"
	"gitlab.com/tirava/nfcc/pkg/errors"
	"gitlab.com/tirava/nfcc/pkg/jobs"
	"gitlab.com/tirava/nfcc/pkg/tags"
)

const endPoint = "/events"

// Events base type.
type Events struct {
	fullURL string
}

// New returns new events.
func New(server string) *Events {
	return &Events{
		fullURL: fmt.Sprintf("%s%s", server, endPoint),
	}
}

// ListAll returns list of events (parameters - optional).
func (e Events) ListAll(eventName, adapterID string,
	limit, offset int, sortBy, sortDir string) (*EventList, error) {
	respErr := &errors.Error{
		Message: "not implemented",
		Info: fmt.Sprintf("%s, name=%s, adapter=%s, limit=%d, offset=%d, sortBy=%s, sortDir=%s", e.fullURL,
			eventName, adapterID, limit, offset, sortBy, sortDir),
	}

	return nil, respErr.Err()
}

// Send event to service.
func (e Events) Send(event NewEvent) (*Event, error) {
	if err := e.validateEventData(event.Name, event.Data); err != nil {
		return nil, err
	}

	respErr := &errors.Error{
		Message: "not implemented",
		Info:    fmt.Sprintf("%s, name=%s", e.fullURL, event.Name),
	}

	return nil, respErr.Err()
}

func (e Events) validateEventData(name string, data EventData) error {
	var err error

	var s string

	switch data.(type) {
	case tags.Tag:
		if name != TagDiscovery && name != TagRelease {
			s = "Tag"
		}
	case adapters.Adapter:
		if name != AdapterDiscovery && name != AdapterRelease {
			s = "Adapter"
		}
	case jobs.Job:
		if name != JobSubmitted && name != JobActivated && name != JobDeleted && name != JobFinished {
			s = "Job"
		}
	case runs.JobRun:
		if name != RunStarted && name != RunSuccess && name != RunError {
			s = "JobRun"
		}
	default:
		return errors.NFCCError("invalid event data type")
	}

	if s != "" {
		err = errors.NFCCError(fmt.Sprintf("Event name '%s' not equal '%s' data type", name, s))
	}

	return err
}
