package events

// EventName enums.
const (
	TagDiscovery     = "tag_discovery"
	TagRelease       = "tag_release"
	AdapterDiscovery = "adapter_discovery"
	AdapterRelease   = "adapter_release"
	JobSubmitted     = "job_submitted"
	JobActivated     = "job_activated"
	JobDeleted       = "job_deleted"
	JobFinished      = "job_finished"
	RunStarted       = "run_started"
	RunSuccess       = "run_success"
	RunError         = "run_error"
)
