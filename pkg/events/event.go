package events

// EventData implements event data.
type EventData interface{}

// Event object.
type Event struct {
	// Unique identifier in form of UUID representing a specific event.
	EventID string `json:"event_id"`
	// Event name.
	Name string `json:"name"`
	// Unique identifier in form of UUID representing a specific adapter.
	AdapterID string `json:"adapter_id"`
	// Adapter name.
	AdapterName string `json:"adapter_name"`
	// Event data one of the types: Tag, Adapter, Job, JobRun
	Data EventData `json:"data"`
	// CreatedAt in date-time format
	CreatedAt string `json:"created_at"`
}

// EventList object.
type EventList struct {
	// Total number of events.
	Total int `json:"total"`
	// Number of events in list.
	Length int `json:"length"`
	// Limit for number of events in list.
	Limit int `json:"limit"`
	// Offset for list.
	Offset int `json:"offset"`
	// Event array.
	Items []Event `json:"items"`
}

// NewEvent object.
type NewEvent struct {
	// Event name.
	Name string `json:"name"`
	// Unique identifier in form of UUID representing a specific adapter.
	AdapterID string `json:"adapter_id"`
	// Event data one of the types: Tag, Adapter, Job, JobRun
	Data EventData `json:"data"`
}
